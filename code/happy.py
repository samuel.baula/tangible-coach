#Required imports
from matrix_lite import led
import time
from time import sleep

#Initialisation
everloop = ['black'] * led.length
everloop[2] = {'r':255, 'g':200, 'b':10, 'w':50 }
everloop[17] = {'r':255, 'g':200, 'b':10, 'w':50 }
everloop[8] = {'r':255, 'g':200, 'b':10, 'w':50 }
everloop[9] = {'r':255, 'g':200, 'b':10, 'w':50 }
everloop[10] = {'r':255, 'g':200, 'b':10, 'w':50 }
everloop[11] = {'r':255, 'g':200, 'b':10, 'w':50 }

everloop.append(everloop.pop(0))
led.set(everloop)

#Warning : index of everloop changed due to pop done above (old index 3 is now 2, 0 is now 17, ...)

#init loop var used for state
i = 0

#loop used to change states of the pattern
while True:
	#reset smile to basic one
	everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }
	
	if i > 0 :
			#enlarge smile
    		everloop[6] = {'r':255, 'g':200, 'b':10, 'w':50 }
    		everloop[11] = {'r':255, 'g':200, 'b':10, 'w':50 }
		
	#print(i) 
	i += 1
	
	#reset state to 0 when last state reached
	if i == 2 :
		i = 0
	
	#light up LEDs and wait 2 seconds for next step of the loop
	led.set(everloop)
	sleep(2)
	

