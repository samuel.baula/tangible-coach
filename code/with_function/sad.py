#Required imports
from matrix_lite import led
import time
from time import sleep

#Initialisation
everloop = ['black'] * led.length
everloop[17] = {'r':80, 'g':80, 'b':255, 'w':0 }
everloop[2] = {'r':80, 'g':80, 'b':255, 'w':0 }

everloop.append(everloop.pop(0))
led.set(everloop)
#Warning : index of everloop changed due to pop done above (old index 3 is now 2, 0 is now 17, ...)

#init loop var used for state
i = 0

#loop used to change states of the pattern
def sademotion():

	#reset tears
	everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }

	#light up tears depending state
	if i > 0 :
		everloop[1+i] = {'r':8, 'g':8, 'b':210, 'w':0 }
		everloop[16-i] = {'r':8, 'g':8, 'b':210, 'w':0 }

	#print(i)
	i += 1
	
	#reset state to 0 when last state reached
	if i == 5 :
		i = 0
	
	#light up LEDs and wait 1 seconds for next step of the loop
	led.set(everloop)
	sleep(1)
	

