#Required imports
from matrix_lite import led
import time
from time import sleep

#initialisation
led.set({'r':255, 'g':255, 'b':255, 'w':0 })

#init loop var used for state
i = 0

while True:

	if i == 0 :
    		led.set({'r':255, 'g':255, 'b':255, 'w':0 })

	if i == 1 or i == 7 :
    		led.set({'r':210, 'g':240, 'b':255, 'w':0 })

	if i == 2 or i == 6 :
		led.set({'r':170, 'g':220, 'b':255, 'w':0 })

	if i == 3 or i == 5 :
		led.set({'r':90, 'g':190, 'b':255, 'w':0 })

	if i == 4 :
		led.set({'r':0, 'g':130, 'b':240, 'w':0 })

	#print(i)
	i += 1

	#reset state to 0 when last state reached
	if i == 8 :
		i = 0

	sleep(0.2)

