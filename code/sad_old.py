from matrix_lite import led
import time
from time import sleep

everloop = ['black'] * led.length
everloop[3] = {'r':80, 'g':80, 'b':255, 'w':0 }
everloop[7] = {'r':80, 'g':80, 'b':255, 'w':0 }
everloop[14] = {'r':80, 'g':80, 'b':255, 'w':0 }

everloop.append(everloop.pop(0))
led.set(everloop)

i = 0

while True:
	everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[1] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[8] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[9] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[10] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[7] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[17] = {'r':0, 'g':0, 'b':0, 'w':0 }
	
	if i > 0 :
		everloop[2-i] = {'r':8, 'g':8, 'b':210, 'w':0 }
		everloop[6+i] = {'r':8, 'g':8, 'b':210, 'w':0 }
		
	everloop[2] = {'r':80, 'g':80, 'b':255, 'w':0 }
	print(i) 
	i += 1
	
	if i == 4 :
		i = 0
		
	led.set(everloop)
	sleep(1)
	
