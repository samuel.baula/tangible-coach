from matrix_lite import led
import time
from time import sleep

everloop = ['black'] * led.length
everloop[3] = {'r':255, 'g':0, 'b':0, 'w':0 }
everloop[7] = {'r':255, 'g':0, 'b':0, 'w':0 }
everloop[14] = {'r':255, 'g':0, 'b':0, 'w':0 }

everloop.append(everloop.pop(0))
led.set(everloop)

i = 0

while True:
	everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
	everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
	
	if i > 0 :
    		everloop[3] = {'r':255, 'g':0, 'b':0, 'w':0 }
    		everloop[5] = {'r':255, 'g':0, 'b':0, 'w':0 }
		
	print(i) 
	i += 1
	
	if i == 2 :
		i = 0
		
	led.set(everloop)
	sleep(0.5)
	
